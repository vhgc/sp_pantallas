$(document).ready(function(){
    
    //  Se agrega mensaje de alerta
    mensajeAlertaAdd();
    
    //  Se reconoce cuando el usuario presiona la tecla enter en el input de password
    $('#in_password').keyup(function(event){
        //  Si el usuario ha tecleado el enter
        if(event.keyCode == 13){
            //  Se ejecuta el evento click del botón entrar.
            $("#btn_entrarlogin").click();
        }
    });
    
    //  Se detecta el click en el botón entrar
    $('#btn_entrarlogin').click(function(){
        
        //  Se valida la longitud del input de usuario, en caso de que sea menor a 3, resalta en rojo el input
        if($('#in_usuario').val().length < 3){
            $('#err_camp_usuario').show();
            return rBorde('in_usuario', true);
        } else {
            //  En caso de que el texto ingresado sea mayor a 4, se quita borde
            $('#err_camp_usuario').hide();
            rBorde('in_usuario', false);
        }
        
        //  Se valida la longitud del input de password, en caso de que sea menor a 3, resalta en rojo el input
        if($('#in_password').val().length < 3){
            $('#err_camp_password').show();
            return rBorde('in_password', true);
        } else {
            //  En caso de que el texto ingresado sea mayor a 4, se quita borde
            $('#err_camp_password').hide();
            rBorde('in_password', false);
        }
        
        //  Se toma el valor de input "in_usuario", en caso de que cumplan con la longitud minima
        var val_usuario = $('#in_usuario').val();
        console.log(val_usuario);
        
        //  Se toma el valor de input "in_password", en caso de que cumplan con la longitud minima
        var val_password = $('#in_password').val();
        console.log(val_password);
        
        //  Se crea variable json donde se almacena información de usuario.
        var datosUsuario = {
            username : val_usuario,
            password : val_password
        };
        
        //  Se deben de enviar los datos, para realizar la petición POST a microservicio...
        $.ajax({
            type : 'POST',
            url : '/login',
            data : datosUsuario,
            dataType : 'json',
            success : function(res, status, xhr){
                //  En caso de que la petición hacia el servidor con la información de usuario
                //  haya sido correcta, se redirigijá a la pantalla que le corresponde.
                window.location.href =  res.pantalla;
                
            },
            error : function(xhr, status, error){
                //  En caso de haya existido un error durante la petición, se deberá de informar al 
                //  usuario del error ocurrido. Mostrando una ventana emergente con la información
                //  que ha sido recibida.
                var accion = "Cerrar";
                var error_msj = JSON.parse(xhr.responseText);
                
                cargarmensajeAlerta('inicio de sesión', error_msj.detail, accion);
                
            }
            
        });     
        
    });
    
    //  Función que resalta input en caso de que no tenga información antes de enviar los datos de usuario 
    function rBorde(control, tipo){
        //  if tipo viene verdadero, significa que el campo no cumple con la condición por lo que se deberá de
        //  recalcar el campo de color rojo
        if(tipo) {
            document.getElementById(control).style.borderWidth = '3px';
            document.getElementById(control).style.borderColor = 'red';
         } else {
             // En caso de que ya cumpla con la condición, se debe poner el formato original.
            document.getElementById(control).style.borderWidth = '1px';
            document.getElementById(control).style.borderColor = '#FFF';
         }
     }
    
});