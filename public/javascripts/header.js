/****************************************************************************************************************
*   Archivo javascript, que se encarga de gestionar los eventos relacionados con el header. Más específicamente
*   para la imagen de usuario y manejo de cierre de sesión.
****************************************************************************************************************/
$(document).ready(function(){
    
    var error_msj;
    
    //  Se detecta click en la imagen de usuario -header-
    $('#img_usuario').click(function(){
        //console.log('click en img_usuario header.js');
        if($('#logOutUser').css('display') == 'none'){
           $('#logOutUser').show();
        } else {
            $('#logOutUser').hide();
        }
    });
    
    //  Botón de logout
    $('#bnt_logout').click(function(){
        
        //  Se oculta div de opciones de usuario
        $('#logOutUser').hide();
        
        $.ajax({
            type : 'POST',
            url : '/salir',
            dataType : 'json',
            success : function(res, status, xhr){
                //  Si la petición de cierre de sesión fue exitosa, la aplicación se redirecciona a la pantalla de inicio
                window.location.href =  res.pantalla;
            },
            error : function(xhr, status, error){
                
                if(xhr.status == 401){
                    //  Nos indica que el usuario no está autorizado, por lo que, debe de redirigir al usuario
                    //  a la pantalla de login.
                    //  <-- Mostrar ventana informando a usuario -->
                    
                    error_msj = JSON.parse(xhr.responseText);
                    
                    //  Se muestra alert, con mensaje a usuario, indicandole el error
                    $('#msjAlerta').show();
                    
                    $('#p_titulo_msjAlerta').text('Error cerrar sesión');
                
                    //  Se pone el texto de error que fue devuelto.
                    $('#p_text_msjAlerta').text(error_msj.detail);
                    
                    //  Después del tiempo especificado, se redirige a la pantalla enviada desde backend
                    setTimeout(function(){
                        window.location.href = error_msj.pantalla;
                    },2000);
                    
                    
                } else {
                    //  <... ver que es lo que pasa cuando sucede un error en cerrar sesión... >
                }
                
            }
            
        });   
        
    });
    
    //  Se detecta click en el botón "regresar"
    $('#btn_regresar').click(function(){
        console.info('Se dio click en el botón regresar ' + paginaCargada);
        if(paginaCargada == 'pagar'){
            //  Si la pantalla es pagar, se debe de validar el rol que tiene el usuario
            //  para conocer a que pantalla se va a redireccionar
            $.ajax({
                type : 'POST',
                url : '/validaRegresoPagar',
                data : { pagina : paginaCargada },
                dataType : 'json',
                success : function(res, status, xhr){
                    //  Devuelve la pantalla a la cual se tiene que redireccionar.
                    window.location.href =  res.pantalla;
                },
                error : function(xhr, status, error){
                    //  Marco en error al consultar la información del usuario.
                }
            });
        } else if (paginaCargada == 'menu'){
            //  La pantalla es menú, se redirecciona a tiempos....
            window.location.href =  'tiempo';
            
        } else if (paginaCargada == 'tiempo'){
            //  Es la pantalla de tiempos, se redirecciona a mesas....
            window.location.href =  'mesas';
        }
        
    });
    
});